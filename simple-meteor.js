Articles = new Mongo.Collection("articles")

if (Meteor.isClient) {

  Meteor.subscribe("articles");

  // Meteor.call("checkArxiv", function(error, results) {
  //   if (error) {
  //       console.log(error);
  //     }
  //   xml2js.parseString(results, function(err, resp) {
  //         if (err) {
  //           console.log(err);
  //         }
  //         console.log(resp)

  //   });

  // });
  // This code only runs on the client
  Template.body.helpers({
    articles: function () {
        return Articles.find({}, {sort: {createdAt: -1}});
    }, 
    savedCount: function () {
        return Articles.find({}).count();
      }

  });

  Template.body.events({
    "submit .new-title": function(event) {

      event.preventDefault();
      var title_text = event.target.title.value;
      var summary_text = "summaryfoo";
      var author_text = "authorfoo";
      var href_text = "hreftext";

      Meteor.call("addArticle",title_text,summary_text, author_text, href_text)
      event.target.title.value = "";
    }
  });

    Template.article.helpers({
        isOwner: function() {
          return this.owner === Meteor.userId();
        }
    });

    Template.article.events({
      "click .toggle-checked": function() {
            Meteor.call("setChecked", this._id, ! this.checked);
      },
      "click .delete": function() {
        Meteor.call("deleteArticle", this._id);
      },
      "click .toggle-private": function() {
        Meteor.call("setPrivate", this._id, ! this.private);
      }
    });

  Accounts.ui.config({
    passwordSignupFields: "USERNAME_ONLY"
  });
}

Meteor.methods({
  addArticle: function (title_text, summary_text, author_text, href_text) {
    if (! Meteor.userId()) {
      throw new Meteor.Error("not-authorized");
    }

      Articles.insert({
        title: title_text,
        summary: summary_text,
        author: author_text,
        href_link: href_text, 
        createdAt: new Date(),
        owner: Meteor.userId(),
        username: Meteor.user().username
      });
  },
  deleteArticle: function(articleId) {
    var article = Articles.findOne(articleId);
    if (article.private && article.owner !== Meteor.userId()) {
        throw new Meteor.Error("not-authorized");      
    }
    Articles.remove(articleId);
  },
  setChecked: function(articleId, setChecked) {
    var article = Articles.findOne(articleId);
    if (article.private && article.owner != Meteor.userId()) {
        throw new Meteor.Error("not-authorized");
    }
    Articles.updated(articleId, {$set: {checked: setChecked} });
  },
  setPrivate: function(articleId,setToPrivate) {
    var article = Articles.findOne(articleId);

    if (article.owner !== Meteor.userId()) {
        throw new Meteor.Error("not-authorized");
    }

    Articles.update(articleId, {$set: {private: setToPrivate } });

  }
});


if (Meteor.isServer) {
  Meteor.methods({
    // checkArxiv: function () {
    //   this.unblock();
    //       var website = "http://export.arxiv.org/api/query?search_query";
    //       var pars = {'cat': 'astro-ph'};
    //       var response = HTTP.call("GET", "http://export.arxiv.org/api/query?search_query=all:electron&start=0&max_results=1") 
    //       xml2js.parseStringSync(response, function(error, results) {
    //           if (error) {
    //             console.log(error);
    //           }
    //           console.log(results.content);

    //       });          


    //       return 
    //   },
      testArxiv: function() {
        var feedData = Scrape.website("http://www.bbc.com/news/technology-31565368")
        feedData.title
        return feedData
      }

  });

  Meteor.publish("articles", function () {
    return Articles.find({
      $or: [
        { private: {$ne: true} },
        { owner: this.userId }
      ]
    });
  });
}
